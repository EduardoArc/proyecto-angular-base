import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/not-auth.guard';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
 
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'auth',canActivate: [NotAuthGuard] ,loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  { path: 'admin',canActivate: [AuthGuard] , data: { breadcrumb: {skip : true}} , loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
  { path: 'investigador',canActivate: [AuthGuard] , data: { breadcrumb: {skip : true}} , loadChildren: () => import('./investigador/investigador.module').then(m => m.InvestigadorModule) },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

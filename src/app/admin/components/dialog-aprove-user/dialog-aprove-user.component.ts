import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-dialog-aprove-user',
  templateUrl: './dialog-aprove-user.component.html',
  styleUrls: ['./dialog-aprove-user.component.css']
})
export class DialogAproveUserComponent implements OnInit {

  user?: any = {};

  constructor(@Inject(MAT_DIALOG_DATA) public data: string, public userService: UsuariosService ,
   public dialogRef: MatDialogRef<any>, ) { 
    this.user = data;
  }

  ngOnInit(): void {
  }

  onAproveClick(){
    this.userService.aprove({id : this.user.id}).subscribe({
      next:(res)=>{
        console.log('res' ,res);
       this.dialogRef.close();
       window.location.reload();
      }
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

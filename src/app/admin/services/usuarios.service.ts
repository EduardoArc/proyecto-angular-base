import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  _controllerUser:string;

  constructor(private genericServices: GenericService) { 
    this._controllerUser = "/user"
  }

  getUsers(){
    return this.genericServices.getGeneric( this._controllerUser , '/solicitudes');
  }

  aprove(body : any){
    return this.genericServices.postGeneric( this._controllerUser , '/active' , body);
  }
}

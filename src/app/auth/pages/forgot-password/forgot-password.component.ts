import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  emailForm: FormGroup;

  

  emailRegex =
    '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(private readonly fb: FormBuilder ,  private readonly router: Router, private authService : AuthService,private _snackBar: MatSnackBar) {
    
    this.emailForm = this.fb.group({

      email: ['', [Validators.required, Validators.pattern(this.emailRegex)]],


    })

  }


  ngOnInit(): void {

  }

  onSubmit():void {
    console.log('llama al servicio')
    this.authService.forgot(this.emailForm.value).subscribe({
      
      next:(res)=>{
        console.log("RESPUESTA DE SERVICIO")
        console.log(res.message);
        this._snackBar.open(res.message + ' ✅ ', "OK");
       this.router.navigate(['auth/login'])
      },

      error:(res)=>{
        
       this._snackBar.open(res.error.message + ' ⚠️', "OK");
      }

      
    })
  }

  goToLogin():void{
    this.router.navigate(['auth/login'])
  }

}

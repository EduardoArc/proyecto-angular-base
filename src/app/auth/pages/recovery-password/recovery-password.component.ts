import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.css']
})
export class RecoveryPasswordComponent implements OnInit {

  token! : string;
  hide = true;
  hideConfirm = true
  passwordForm: FormGroup;

  constructor(private readonly fb: FormBuilder, 
    private readonly router: Router, 
    private readonly route: ActivatedRoute,
    private authService : AuthService,
    private _snackBar: MatSnackBar
    ) { 



    this.passwordForm = this.fb.group({
     
      password: ['', [Validators.required]],
      password_confirm: ['', [Validators.required ] ],
    


    }, 
  //  { validators: this.passwordMatchingValidatior }
    )

    // tslint:disable-next-line:no-non-null-assertion
    this.passwordForm!
      .get('password_confirm')!
      // tslint:disable-next-line:no-non-null-assertion
      .setValidators([Validators.required, CustomValidators.equals(this.passwordForm.get('password')!)]);
  }

  ngOnInit(): void {
    
    this.route.params.subscribe((params: Params) => {
      this.token = params['token'];
    })

    

  }

  onSubmit(): void {
    console.log('Form ->', this.passwordForm.value)
    //TODO:: llamar al servicio -> enviar form y token por parametro
    this.authService.newPassword(this.passwordForm.value, this.token).subscribe({
      
      next:(res)=>{
        console.log("RESPUESTA DE SERVICIO")
        console.log(res);
        this.router.navigate(['auth/login']);
        this._snackBar.open(res.message, "OK");
      },

      error:(res)=>{
        console.log("ERROR o no" , res)
        
       this._snackBar.open(res.error.message, "OK");
      }

      
    })
  }

}

function equalsValidator(otherControl: AbstractControl): ValidatorFn {

  return (control: AbstractControl): { [key: string]: any } | null => {
    const value: string = control.value;
    const otherValue: any = otherControl.value;
    return otherValue === value ? null : {notEquals: {value, otherValue}};
  };
} 


export const CustomValidators = {
  equals: equalsValidator
};
